# Genetic Learning Simulation #
- - - -

## Quick Summary ##
The goal of this genetic learning simulation is to simulate some sort of natural selection process and see emergent behavior.

## Stimuli and General Simulation Flow Overview ##

The stimuli enum is in enums.cs. The following stimuli are currently in use:

- sightEnemy

    Fires if we see an enemy entity.

- sightFriend

    Fires if we see a friend.

- sightFood

    Fires if we see food.

- sightUnknown

    Fires if we see something we do not recognize.

- painEnemy

    Fires if we are hurt by an enemy.

- painEnvironment

    Fires if we are hurt by our environment.

- painFriend

    Fires if we are hurt by a friend.

- painFood

    Fires if we are hurt by our food.

- painUnkown

    Fires if we are hurt by an unknown cause.