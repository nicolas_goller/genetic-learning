﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grub : MonoBehaviour, Entity
{
    public int[] stimuli;
    public Stats stats;
    public bool dead = false;
    public float health = 1f;
    public float size = 1f;
    public float decayTime = 5f;

    [Header("Components")]
    [SerializeField]
    private CircleCollider2D vision;
    [SerializeField]
    private CircleCollider2D selfCollider;
    [SerializeField]
    private Rigidbody2D myRigidBody;
    [SerializeField]
    private Transform myTransform;
    private Entity friend, flee, attacker, enemy, food, unknown;
    private Genome genome = new Genome();
    private Action actionToTake;
    private Vector2 destination;
    
    public void Start()
    {
        genome.GenerateRandomGenome();
        actionToTake = Action.wander;
        stimuli = new int[Enum.GetNames(typeof(Stimuli)).Length];
    }

    public void FixedUpdate()
    {
        if (!dead)
        { 
            var stimuliTotal = 0;
            //evaluate stimuli and take an action
            for (var i = 0; i < stimuli.Length; i++)
            {
                stimuliTotal += 1 << i;
            }

            actionToTake = genome.getAction(stimuliTotal);

            switch (actionToTake)
            {
                case Action.attackEnemy:
                    Attack(enemy);
                    break;
                case Action.attackFriend:
                    Attack(friend);
                    break;
                case Action.attackUnknown:
                    Attack(unknown);
                    break;
                case Action.eatEnemy:
                    Eat(enemy);
                    break;
                case Action.eatFood:
                    Eat(food);
                    break;
                case Action.eatFriend:
                    Eat(friend);
                    break;
                case Action.eatUnkown:
                    Eat(unknown);
                    break;
                case Action.flee:
                    Flee(flee);
                    break;
                case Action.split:
                    Split();
                    break;
                case Action.stay:
                    Stay();
                    break;
                case Action.wander:
                    Wander();
                    break;
            }
        }

        //reset the stimuli we'll assume everything is reset appropriately in the ensuing frame.
        for (var i = 0; i < stimuli.Length; i++)
        {
            stimuli[i] = 0;
        }
        //friend = null; enemy = null; unknown = null; food = null; flee = null;
    }


    //********interface functions********

    public void Attack(Entity e)
    {
        if (e != null && e.Alive())
        {
            e.DefendAttack(this);
        }
    }

    public void Eat(Entity e)
    {
        if (e != null && e.Edible())
        {
            health += e.Consume();
        }
    }

    public void Flee(Entity e)
    {
        //move in direction opposite of this noob.
    }

    public void Split()
    {
        //split in half...half your health...share genome but chance for random variation.
    }

    public void Stay()
    {
        //for now do nothing
        destination = transform.position;
    }

    public void Wander()
    {
        if (destination.sqrMagnitude > 0) { }
        //destination = vision.radius;
    }

    public bool DefendAttack(Entity attacker)
    {
        this.attacker = attacker;
        //you got hit...eventually maybe use speed to dodge or something
        return true;
    }

    private void OnTriggerStay2D(Collider2D collider2d)
    {
        Entity entityInQuestion = collider2d.gameObject.GetComponent<Entity>();

        switch (collider2d.tag)
        {
            case "grub":
                friend = entityInQuestion;
                stimuli[(int)Stimuli.sightFriend] = 1;
                break;
            case "food":
                food = entityInQuestion;
                stimuli[(int)Stimuli.sightFood] = 1;
                break;
            case "enemy":
                enemy = entityInQuestion;
                stimuli[(int)Stimuli.sightEnemy] = 1;
                break;
            case "unknown":
                unknown = entityInQuestion;
                stimuli[(int)Stimuli.sightEnemy] = 1;
                break;
        }
    }

    public bool Edible()
    {
        return !Alive();
    }

    public bool Alive()
    {
        return !dead;
    }

    public float Consume()
    {
        dead = true;
        GameObject.Destroy(gameObject, decayTime);
        return size;
    }
}
