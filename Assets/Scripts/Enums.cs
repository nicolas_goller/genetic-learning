﻿public enum Stimuli
{
    sightEnemy,
    sightFriend,
    sightFood,
    sightUnknown,
    painEnemy,
    painEnvironment,
    painFriend,
    painFood,
    painUnkown,
    //tasteFood,
    //tasteUnkown
}

public enum Action
{
    wander,
    split,
    eatFriend,
    eatEnemy,
    eatUnkown,
    eatFood,
    attackEnemy,
    attackFriend,
    attackUnknown,
    stay,
    flee,
}

public interface Entity
{
    void Wander();
    void Split();
    void Eat(Entity e);

    void Attack(Entity e);
    //used to assess if hit or not by attack?
    bool DefendAttack(Entity e);

    void Stay();
    void Flee(Entity e);
    bool Edible();
    bool Alive();
    float Consume();
}