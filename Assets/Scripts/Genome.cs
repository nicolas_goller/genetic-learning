﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Genome
{
    private int[] genome = new int[1 << Enum.GetNames(typeof(Stimuli)).Length];

    public Genome() { }

    public void GenerateRandomGenome()
    {
        var length = Enum.GetNames(typeof(Action)).Length;
        for (var i = 0; i < genome.Length; i++)
        {
            genome[i] = UnityEngine.Random.Range(0, length);
        }
    }

    public Action getAction(int stimuliTotal)
    {
        return (Action)genome[stimuliTotal];
    }
}
