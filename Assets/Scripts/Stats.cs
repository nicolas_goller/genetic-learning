﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stats {
    public int power;
    public int speed;
    public int toughness;
    public int growthRate;

    public Stats(int power, int speed, int toughness, int growthRate) {
        this.power = power;
        this.speed = speed;
        this.toughness = toughness;
        this.growthRate = growthRate;
    }
}